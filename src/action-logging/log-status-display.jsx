import React from 'react';
import classNames from 'classnames';
import ReactTooltip from 'react-tooltip';
import PropTypes from 'prop-types';

import styles from './log-status-tooltip.css';
import comingSoonStyles from '../components/coming-soon/coming-soon.css';

import {wsIsOpen, wsHasSaveError} from 'scratch-log-sender';

class LogStatusDot extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            wsReady: wsIsOpen(),
            saveError: wsHasSaveError()
        };
    }

    componentDidMount () {
        this.interval = setInterval(() => this.updateStatus(), 1000);
    }

    componentWillUnmount () {
        clearInterval(this.interval);
    }

    updateStatus () {
        this.setState({wsReady: wsIsOpen()});
        this.setState({saveError: wsHasSaveError()});
    }

    render () {
        let dotColor = 'DarkGreen';
        if (this.state.saveError) dotColor = 'DarkOrange';
        if (!this.state.wsReady) dotColor = 'DarkRed';
        return (
            <React.Fragment>
                <span
                    className={styles.coloredCircle}
                    style={{backgroundColor: dotColor}}
                />
            </React.Fragment>
        );
    }
}

const getLogStatusMessage = () => {
    const connectedMsg = 'Connected to logging endpoint';
    const notConnectedMsg = 'Not connected to logging endpoint';
    const saveErrorMsg = 'Logging endpoint not connected to database';
    if (!wsIsOpen()) return (notConnectedMsg);
    if (wsHasSaveError()) return (saveErrorMsg);
    return (connectedMsg);
};

const LogStatusTooltip = props => (
    <React.Fragment>
        <ReactTooltip
            className={classNames(comingSoonStyles.comingSoon, comingSoonStyles.bottom)}
            id={props.tooltipId}
            getContent={getLogStatusMessage}
            place={'bottom'}
        />
    </React.Fragment>
);

LogStatusTooltip.propTypes = {
    tooltipId: PropTypes.string.isRequired
};

const LogStatusDisplay = () => (
    <React.Fragment>
        <div>
            <div
                className={'log-status-dot'}
                data-tip="tooltip"
                data-for={'log-status'}
            >
                <LogStatusDot />
            </div>
            <LogStatusTooltip
                tooltipId={'log-status'}
            />
        </div>
    </React.Fragment>
);

export default LogStatusDisplay;
